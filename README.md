## This app pops every hour to prompt user to log daily activities.

Todo:

1. display logged information on index page
1. search, sort by day, week, or month
1. When notification is clicked, app should popup with the form page ready for user to enter info.
1. when the user enters info and clicks save, the user is directed to the log page
1. also, user should be able to set how often the popup occurs
