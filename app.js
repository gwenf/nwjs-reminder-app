var gui = require('nw.gui');

var fs = require('fs');
var os = require('os');

var win = gui.Window.get();
var formWin;

//set this to run every hour or on some other interval
var cron = require('node-cron');
cron.schedule('*/1 * * * *', function(){
  console.log('running a task every two minutes');
});

//TODO: this function will read the contents of the activities.json file and display
// them on the index.html main page
function loadLog() {

    fs.readFile('activities.json', (err, data) => {
        if (err) throw err;
        alert(data);
    });
}

var options = {
    icon: "img/printer.jpg",
    body: "Click here to log your activity."
};
var notification = new Notification("What are you working on right now?", options);

notification.onclick = function () {

    formWin = gui.Window.open('form.html', {
        focus: true
    });
}

notification.onshow = function () {
    var audio = new Audio('media/piano-f-minor-chord.wav');
    // audio.play(); //comented out for development
    // TODO: Should I use this:
    // setTimeout(function() {notification.close();}, 15000);
}
