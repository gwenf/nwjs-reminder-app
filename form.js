var gui = require('nw.gui');

var fs = require('fs');
var os = require('os');

var win = gui.Window.get();

function saveActivity () {
    var input = document.getElementById('activity-input').value;
    var activities = [];

    try {
        var activityFile = fs.readFileSync('activities.json');
        var activities = JSON.parse(activityFile);
    } catch (e) {
        console.log(e);
    }

    activities.push({date: new Date(), activity: input});
    fs.writeFileSync('activities.json', JSON.stringify(activities));

    win.close();
    // win.focus();
    // loadLog();
}
